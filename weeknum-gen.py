'''
==== WEEK NUMBER GENERATOR ====
Author: Simon-Pierre Brazeau
Date:   July 28, 2017

This script generates a string containing the two last digits of the current
year, followed by the letter 'w' and the current week number. If a string of 
characters is given as an argument, the script adds it to the end of the
generated string. The script the prints out the generated string.
'''

import datetime
import sys

def generateWeekNum (suffix = ''):
    # Get array with current date in iso format
    iso = datetime.date.today().isocalendar()

    # Get last 2 characters of year
    y = str(iso[0])[-2:]

    # Get week number (make sure it spans 2 characters)
    w = str(iso[1]).rjust(2, '0')

    # Generate week number string with suffix
    wn = y + 'w' + w + suffix

    return wn


if __name__ == '__main__':
    arg = ''

    if len(sys.argv) > 1:
        arg = sys.argv[1]

    print(generateWeekNum(arg))

