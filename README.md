# Week Number Generator
This script generates a string containing the two last digits of the current
year, followed by the letter 'w' and the current week number. If a string of 
characters is given as an argument, the script adds it to the end of the
generated string. The script the prints out the generated string.
